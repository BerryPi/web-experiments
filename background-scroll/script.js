const bgX = document.getElementById("position-x");
const bgY = document.getElementById("position-y");
const bgScale = document.getElementById("background-scale");
const transitionSpeed = document.getElementById("transition-speed");

function setParameters() {
  document.body.style.backgroundPositionX = bgX.value;
  document.body.style.backgroundPositionY = bgY.value;
  document.body.style.backgroundSize = bgScale.value;
  document.body.style.transitionDuration = transitionSpeed.value;
}

document.getElementById("submit").addEventListener("click", setParameters);