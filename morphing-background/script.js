var backgroundTiles = [];

function prepareBackground() {
   // Tile the background with bg-tile
   const viewWidthPx = document.documentElement.clientWidth;
   const viewHeightPx = document.documentElement.clientHeight;

   // Assumes the CSS variable is set in pixels
   const tileSizePx = parseFloat(getComputedStyle(document.body).getPropertyValue('--tile-size'));

   let tileTemplate = document.getElementsByClassName("bg-tile")[0];
   let background = document.getElementById("bg-tiles");

   // The tiling should go past just the screen space, as a buffer
   // to allow for some background effects to move it around a bit.
   const BUFFER_FACTOR = 1.5;
   // In general these won't be whole numbers; that's fine, we can just
   // overshoot them in the loop.
   var numRows = (viewHeightPx / tileSizePx) * BUFFER_FACTOR;
   var numCols = (viewWidthPx / tileSizePx) * BUFFER_FACTOR;

   for (let i = 0; i < numRows * numCols; i++){
      let newTile = tileTemplate.cloneNode();
      background.appendChild(newTile);
   }
   backgroundTiles = document.getElementsByClassName("bg-tile");
}

function setShape(shape){
   for (const tile of backgroundTiles) {
      tile.className = `bg-tile ${shape}`
   }
}

prepareBackground();