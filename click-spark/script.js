document.onclick = (event) => {
    // Main spark container
    const mainSpark = document.createElement("div");
    mainSpark.classList.add("spark", "spark-main");
    mainSpark.style.top = `${event.clientY}px`;
    mainSpark.style.left = `${event.clientX}px`;

    const spark1Vertical = document.createElement("div");
    spark1Vertical.classList.add("spark-particle-1-vertical");
    const spark1 = document.createElement("div");
    spark1.classList.add("spark", "spark-particle-1");

    const spark2Vertical = document.createElement("div");
    spark2Vertical.classList.add("spark-particle-2-vertical");
    const spark2 = document.createElement("div");
    spark2.classList.add("spark", "spark-particle-2");

    spark1Vertical.append(spark1);
    spark2Vertical.append(spark2);
    mainSpark.append(spark1Vertical);
    mainSpark.append(spark2Vertical);
    document.body.append(mainSpark);

    // Clean up when the animation is done.
    spark2.onanimationend = () => {
        mainSpark.remove();
    }
}