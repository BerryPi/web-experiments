const tile = document.getElementsByClassName('bg-tile')[0];

// We want the tile to immediately scale up, then gradually scale back down.
// This is done adding a CSS class to set the scale, then removing it and adding
// a class that resets the scale back to 1 but adds a transition delay, then
// removing that class so the element returns to a neutral state.

// Unfortunately, we can't do this as a simple sequence inside a function as the
// display is not recalculated in between.
// We need to wait for the changes to be applied, which is done by adding an event
// handler for the end of a transition, and using it to add the next class in the
// sequence to the tile.

function pulse() {
    // Wait for a pulse to finish before starting another one.
    if (tile.classList.contains('pulsed') || tile.classList.contains('unpulsed')) {
        return;
    }
    tile.classList.add('pulsed');
    // The event handler will take care of removing this class when the animation is done.
}

function tileTransition() {
    if (tile.classList.contains('pulsed')){
        tile.classList.replace('pulsed', 'unpulsed');
    }
    else if (tile.classList.contains('unpulsed')) {
        tile.classList.remove('unpulsed');
    }
}

tile.addEventListener('transitionend', tileTransition, false);